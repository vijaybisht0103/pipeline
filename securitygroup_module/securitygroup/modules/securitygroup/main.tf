
resource "aws_security_group" "samarth_sg" {
  name        = var.name
  description = var.description
  vpc_id      = data.aws_vpc.vpc-name.id

  dynamic "ingress" {
    for_each = var.inbound
    content {
      description = ingress.value.description
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  dynamic "egress" {
    for_each = var.outbound
    content {
      description = egress.value.description
      from_port   = egress.value.port
      to_port     = egress.value.port
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }
  
  tags = var.tags
}