variable "inbound" {
  description = "inbound rules for the security group"
  type = list(object({
    port        = number
    cidr_blocks = list(string)
    protocol = string
    description = string
  }))
}

variable "outbound" {
  description = "outbound rules for security group"
  type = list(object({
    port          = number
    cidr_blocks = list(string)
    protocol = string
    description = string

  }))
}

variable "tags" {
  description = "a map of tags to add to security group"
  type = map
}

variable "name" {
description = "a name for security group "
type = string
}

variable "description" {
  description = "description about security group"
  type = string
  }

variable "access_key" {
 type = string
}

variable "secret_key" {
type = string
}

